# Created by lchhabriya at 10/10/2018
from bs4 import BeautifulSoup
from beautifulscraper import urllib2

def scrap():
    results = []
    fin = dict()
    description, link = 0, 0
    bidstartdate, bidenddate = 'NA', 'NA'
    url = 'http://procurement-notices.undp.org/index.cfm'
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')
    table = tidy.find('table',{"class":"standard cellborder"})
    tr = table.findAll('tr')
    tr = tr[1:]
    for row in tr:
        td = row.findAll('td')
        i=0
        for data in td:
            i+=1
            if (i == 4):
                description = data.text
                link = "http://procurement-notices.undp.org/" + data.find('a').get('href')
            if (i == 7):
                bidenddate = data.text
            if (i == 8):
                bidstartdate = data.text
        if description != 0:
            fin[description] = link
            results += [[description, link, bidstartdate, bidenddate]]
            description, link = 0, 0
            bidstartdate, bidenddate = 'NA', 'NA'
    return results

