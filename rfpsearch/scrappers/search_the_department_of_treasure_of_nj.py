from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select


def scrap():
    try:
        url = 'https://www1.state.nj.us/TYM_BUISOPP/bo/searchBusinessOpportunities.do'
        # load new url using selenium
        driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
        driver.get(url)
        wait = WebDriverWait(driver, 20)
        results = []
        fin = dict()
        description, link = 0, 0
        bidstartdate, enddate = 'NA', 'NA'
        endtime, enddate = 'NA', 'NA'
        # wait.until(EC.presence_of_element_located(By.NAME,"category"))
        select_element = Select(driver.find_element(By.NAME, 'category'))
        select_element.select_by_value('08')
        driver.find_element(By.XPATH,
                            '/html/body/table/tbody/tr[1]/td[2]/table/tbody/tr[5]/td/table/tbody/tr/td/form/table/tbody/tr[4]/td/input').click()
        # wait.until(EC.presence_of_element_located(By.XPATH('/html/body/table/tbody/tr[1]/td[2]/table/tbody/tr[5]/td/table/tbody/tr[2]/td/a/table/tbody/tr[1]/td/font/strong/text()')))
        bigtable = driver.find_element_by_xpath('/html/body/table/tbody/tr[1]/td[2]/table/tbody/tr[5]/td/table')
        bigtabletbody = bigtable.find_element_by_tag_name('tbody')
        bigtabletr = bigtabletbody.find_elements_by_tag_name('tr')
        bigtabletr = bigtabletr[1:]
        bigtabletr[-1]
        i = 1
        numberofarticlesstring = driver.find_element_by_xpath(
            '/html/body/table/tbody/tr[1]/td[2]/table/tbody/tr[5]/td/table/tbody/tr[2]/td/a/table/tbody/tr[1]')
        numberofarticlesstringtext = numberofarticlesstring.text
        numberofarticlesstringnumber = numberofarticlesstringtext[numberofarticlesstringtext.index('(') + 1:]
        numberofarticlesstringnumber = numberofarticlesstringnumber.rsplit(')')
        limit = int(numberofarticlesstringnumber[0]) * 2
        print(limit)
        for row in range(0, limit):
            i += 1
            if (i % 2 == 0):
                enddate = driver.find_element(By.XPATH,
                                              '/html/body/table/tbody/tr[1]/td[2]/table/tbody/tr[5]/td/table/tbody/tr[' + str(
                                                  i + 1) + ']/td/table/tbody/tr/td[2]/div/font/strong')
                enddate = enddate.text.rsplit('Closing Date: ')
                endtime = driver.find_element(By.XPATH,
                                              '/html/body/table/tbody/tr[1]/td[2]/table/tbody/tr[5]/td/table/tbody/tr[' + str(
                                                  i + 1) + ']/td/table/tbody/tr/td[3]/div/font/strong')
                endtime = endtime.text.rsplit('Closing Time: ')
                bidenddate = enddate[1] + ' ' + endtime[1]

            if (i % 2 != 0):
                description = driver.find_element(By.XPATH,
                                                  '/html/body/table/tbody/tr[1]/td[2]/table/tbody/tr[5]/td/table/tbody/tr[' + str(
                                                      i + 1) + ']/td/table/tbody/tr/td/p[1]/font[1]')
                description = description.text.replace('\n', '').strip()
                description = description.rsplit('Description:')[1]
                bidstartdate = driver.find_element(By.XPATH,
                                                   '/html/body/table/tbody/tr[1]/td[2]/table/tbody/tr[5]/td/table/tbody/tr[' + str(
                                                       i + 1) + ']/td/table/tbody/tr/td/p[2]/font')
                bidstartdate = bidstartdate.text.rsplit('Posted On: ')[1]
                link = driver.find_element(By.XPATH,
                                           '/html/body/table/tbody/tr[1]/td[2]/table/tbody/tr[5]/td/table/tbody/tr[' + str(
                                               i + 1) + ']/td/table/tbody/tr/td/p[3]/font/a')
                link = link.text

            if (description != 0):
                fin[description] = link
                results += [[description, link, bidstartdate, bidenddate]]
                description, link = 0, 0
                bidstartdate, bidenddate = 'NA', 'NA'
                endtime, enddate = 'NA', 'NA'

        driver.quit()
        return results
    except:
        results = [['Website problem,Please contact system administrator']]
        driver.quit()
