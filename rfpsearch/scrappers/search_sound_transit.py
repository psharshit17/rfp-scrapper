# Created by lchhabriya at 11/06/2018

from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
import time


def scrap():
    try:
        url = 'https://www.ebidexchange.com/SolicitationList.aspx?cid=b7ed7fa3-38ea-40d0-854d-357a810a8d41&uid=00000000-0000-0000-0000-000000000000'
        # load new url using selenium
        driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
        driver.get(url)
        wait = WebDriverWait(driver, 20)
        results = []
        fin = dict()
        description, link = 0, 0
        startdate, enddate = 'NA', 'NA'
        status = 'NA'
        k = 2
        time.sleep(2)
        select_element = Select(driver.find_element_by_xpath('//*[@id="cphB_ddFilterStatus"]'))
        select_element.select_by_value('1')
        select_element2 = Select(
            driver.find_element_by_xpath('//*[@id="cphB_GridViewSolicitations_GridViewSolicitationsddp_0"]'))
        select_element2.select_by_value('5000')
        time.sleep(2)
        while True:
            tbody = driver.find_element_by_xpath('//*[@id="cphB_GridViewSolicitations"]/tbody')
            tr = tbody.find_elements_by_tag_name('tr')
            j = 3
            for row in tr:
                wait.until(EC.presence_of_element_located(
                    (By.XPATH, '//*[@id="cphB_GridViewSolicitations"]/tbody/tr[' + str(j) + ']/td[1]'))).click()
                link = driver.current_url
                description = driver.find_element_by_xpath(
                    '//*[@id="SolicitationHeader_lblNewSolicInfoSection"]/font[1]').text
                description = description.split("Solicitation Title: ")[1]
                enddate = driver.find_element_by_xpath(
                    '//*[@id="SolicitationHeader_lblNewSolicInfoSection"]/font[3]').text
                enddate = enddate.split("Bids Due: ")[1]
                status = driver.find_element_by_xpath(
                    '//*[@id="SolicitationHeader_lblNewSolicInfoSection"]/font[4]').text
                if ('Open' in status):
                    fin[description] = link
                    results += [[description, link, startdate, enddate]]
                    description, link = 0, 0
                    startdate, enddate = 'NA', 'NA'
                    status = 'NA'
                driver.back()
                j += 1

            driver.find_element_by_xpath(
            '//*[@id="cphB_GridViewSolicitations"]/tbody/tr[2]/td/table/tbody/tr/td[' + str(k) + ']').click()
            k += 1
    except:
        if(len(results)==0):
            results = [["No RFP to show, either zero RFP's or website error, Please check the original website or contact system administrator"]]
        driver.quit()
        return results
