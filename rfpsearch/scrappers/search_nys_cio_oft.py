# Created by lchhabriya at 10/10/2018
from selenium import webdriver  # To launch/initialise a browser
from selenium.webdriver.support.wait import WebDriverWait  # To wait for a page to load.
from bs4 import BeautifulSoup  # scrapping
from selenium.common.exceptions import NoSuchElementException  # Exception Handling


def scrap():
    results = []
    url = 'http://www.its.ny.gov/competitive-procurement-opportunities'
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    wait = WebDriverWait(driver, 20)
    driver.get(url)
    tidy = BeautifulSoup(driver.page_source, 'html.parser')
    div = tidy.find('div',{"class":"field-item even"})
    p = div.findAll('p')
    fin = dict()
    description, link = 0, 0
    startdate,enddate = 'NA','NA'
    for row in p:
        description = row.text
        link = "https://its.ny.gov" + row.find('a').get('href')
        if description != 0 and link != 0:
            fin[description] = link
            results += [[description, link,startdate,enddate]]
            description, link = 0, 0
            startdate, enddate = 'NA', 'NA'
    driver.quit()
    return results