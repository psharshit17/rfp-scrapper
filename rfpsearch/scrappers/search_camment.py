from bs4 import BeautifulSoup
from beautifulscraper import urllib2


def scrap():
    results = []
    url = 'https://cammnet.octa.net/procurements/'
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')
    span = tidy.find('span',{"id": "interiorRightSideContent_proLabel"})
    spantext = span.contents[0].text
    fin = dict()
    description, link = 0, 0
    for row in span:
        td = row.find_all('td')
        i = 0
        for data in td:
            i += 1
            if i == 1:
                link = "https://cammnet.octa.net" + data.find('a').get('href')
            elif i == 2:
                description = data.text

        if description != 0:
            fin[description] = link
            results += [[description, link]]
    return  results