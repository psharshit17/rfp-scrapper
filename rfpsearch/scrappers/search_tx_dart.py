# Created by lchhabriya at 10/28/2018
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
import time


def scrap():
    url = 'https://suppliers-prod.dart.org/lmproc/SourcingSupplier/lm?bto=SourcingEvent&name=OpenForBid&service=list&_susec=true&webappname=SourcingSupplier&_frommenu=true&dataarea=lmproc'
    # load new url using selenium
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    driver.get(url)
    wait = WebDriverWait(driver, 20)
    results = []
    fin = dict()
    description, link = 0, 0
    startdate,enddate = 'NA','NA'
    status = 'NA'
    element = driver.find_element_by_xpath('//*[@id="pageDown_OpenForBid"]')
    try:
             while True:
                table = driver.find_element_by_xpath('//table[starts-with(@id, "UID0-")]/tbody[@class="listTbody"]')
                tr = table.find_elements_by_tag_name('tr')
                for row in tr:
                    i=0
                    td = row.find_elements_by_tag_name('td')
                    for data in td:
                        i+=1
                        if(i==6):
                            description = data.text

                        if(i==11):
                            startdate = data.text.strip()
                        if(i==12):
                            enddate = data.text.strip()
                        if(i==13):
                            status = data.text.strip()
                        link = url
                    if(status == 'Open'):
                        fin[description] = link
                        results += [[description, link, startdate, enddate]]
                        description, link = 0, 0
                        startdate, enddate = 'NA', 'NA'
                        status = 'NA'
                if(element.is_enabled()):
                    element = element.click()
                    time.sleep(10)
                else:
                    break
                # driver.quit()



    except:
        # if(len(results)!=0):
        #     return results
        # else:
        #     results = [['Please contact system administrator']]
        #     return results
        return results
        driver.quit()

