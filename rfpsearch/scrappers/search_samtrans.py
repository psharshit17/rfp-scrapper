# Created by lchhabriya at 10/19/2018
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC


def scrap():
    url = 'https://smctd.bonfirehub.com/portal/?tab=openOpportunities'
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    wait = WebDriverWait(driver, 20)
    driver.get(url)

    wait.until(EC.presence_of_element_located((By.XPATH,'//*[@id="DataTables_Table_0"]/tbody/tr')))
    tr = driver.find_elements_by_xpath('//*[@id="DataTables_Table_0"]/tbody/tr')

    results = []
    fin = dict()
    description, link = 0, 0
    startdate, enddate = 'NA', 'NA'

    for row in tr:
        tds = row.find_elements_by_tag_name('td')
        i = 0
        for data in tds:
            i+=1
            if(i==3):
                description  = data.text
            if(i==4):
                enddate = data.text

            if(i==6):
                link = data.find_element_by_tag_name('a').get_attribute("href")
        if (description != 0):
            fin[description] = link
            results += [[description, link, startdate, enddate]]
            description, link = 0, 0
            startdate, enddate = 'NA', 'NA'
    driver.quit()
    return results
