from bs4 import BeautifulSoup
from beautifulscraper import urllib2
from urllib.request import urlopen
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import StaleElementReferenceException

def scrap():
    results = []
    url = 'https://www.sanantonio.gov/purchasing/biddingcontract/opportunities'
    #page = urllib2.urlopen(url)
    page = urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')

    # find iframe and get new url
    frame = tidy.find('iframe')
    url = frame.attrs['src']

    # load new url using selenium
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    driver.set_window_size(1024, 768)
    wait = WebDriverWait(driver, 20)
    driver.get(url)

    td = driver.find_elements_by_xpath('//*[@id="GridView1"]/tbody/tr[22]/td/table/tbody/tr/td')
    results = []
    fin = dict()
    description, link, startdate, enddate = 0, 0, 'NA', 'NA'

    for pageNum in td:
        # print(pageNum.text)
        if int(pageNum.text) > 1:
            wait.until(EC.presence_of_element_located((By.LINK_TEXT, pageNum.text))).click()
        tr = driver.find_elements_by_xpath('//*[@id="GridView1"]/tbody/tr')
        for row in tr:
            i = 0
            tds = row.find_elements_by_tag_name('td')
            for data in tds:
                if i == 0:
                    description = data.text
                    link = data.find_element_by_tag_name('a').get_attribute("href")
                i += 1
                if (i == 4):
                    startdate = data.text
                if (i == 6):
                    enddate = data.text
            if description != 0 and link != 0:
                if not link.startswith('javascript'):
                    fin[description] = link
                    results += [[description, link, startdate, enddate]]
                    description, link, startdate, enddate = 0, 0, 'NA', 'NA'

    driver.quit()
    return results