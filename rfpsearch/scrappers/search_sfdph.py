# Created by lchhabriya at 11/05/2018

from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait


def scrap():
    try:
        url = 'https://www.sfdph.org/dph/comupg/aboutdph/insideDept/Contracts/default.asp'
        # load new url using selenium
        driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
        driver.get(url)
        wait = WebDriverWait(driver, 20)
        results = []
        fin = dict()
        description, link = 0, 0
        startdate, enddate = 'NA', 'NA'
        status = 'NA'

        div = driver.find_element_by_xpath('//*[@id="module"]')
        ps = div.find_elements_by_tag_name('p')
        i=0
        link = url
        for row in ps:
            #not using following code because of notice of withdrawal post, so not able to skip that and move on as odd and even calculation problem
            # if(i%2 ==0):
            #     print(i)
            #     # if(row.find_element_by_tag_name('a')):
            #     link = row.get_attribute('href')
            #     print(row.find_element_by_tag_name('a').get_attribute('href'))
            #     if('Withdrawal' not in row.find_element_by_tag_name('a').text):
            #         if(len(ps)< (i+1)):
            #             row = ps[i +1]
            #         else:
            #             row = ps[i]
            #         description = row.text
            #         print(description)
            #         fin[description] = link
            #         results += [[description, link, startdate, enddate]]
            #         description, link = 0, 0
            #         startdate, enddate = 'NA', 'NA'
            #     else:
            #         i+=1
            # i+=1
            if(i%2==1):
                description = row.text
                fin[description] = link
                results += [[description, link, startdate, enddate]]
                description, link = 0, 0
                startdate, enddate = 'NA', 'NA'
            i+=1

        driver.quit()
        return results
    except:
        results = [['Website error please contact system administrator']]
        driver.quit()
        return results
