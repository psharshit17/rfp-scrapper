"""
@author: Lchhabriya
"""
from bs4 import BeautifulSoup
from beautifulscraper import urllib2

def scrap():
    results = []
    url = 'http://www.saws.org/business_center/procbids/'
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')
    div = tidy.find('div', {"id": "main_content"})
    alltable = div.findAll('table')

    # To get the number of RFPs and we can use for double check with the results
    lasttable = alltable[-1]
    lasttd = lasttable.find('td').getText()
    location = lasttd.index('of') + 2
    totalnumberofbids = lasttd[location:].strip()

    # To get the actuals records
    table = alltable[2]
    tr = table.find_all('tr')
    tr = tr[1:]
    fin = dict()
    description, link = 0, 0
    bidstartdate, bidduedate = 'NA', 'NA'
    for row in tr:
        td = row.find_all('td')
        i = 0
        for data in td:
            i += 1
            if i == 1:
                type = data.text.strip()
            if i == 3:
                description = data.text.strip()
            if i == 4:
                bidduedate = data.text.strip()
            if i == 6:
                link = "http://www.saws.org/business_center/procbids/" + data.find('a').get('href')
        if description != 0:
            description = type + ' - ' + description
            fin[description] = link
            results += [[description, link, bidstartdate, bidduedate]]
            description, link = 0, 0
            bidstartdate, bidduedate = 'NA', 'NA'
    return results