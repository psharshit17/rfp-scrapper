# Created by lchhabriya at 10/16/2018
import xlrd
from selenium import webdriver  # To launch/initialise a browser
from selenium.webdriver.support.wait import WebDriverWait  # To wait for a page to load.
from bs4 import BeautifulSoup  # scrapping

def scrap():
    results = []
    fin = dict()
    description, link = 0, 0
    startdate,enddate = 'NA','NA'
    username = 'NA'
    paswwrod = 'NA'
    category = 'NA'

    workbook = xlrd.open_workbook("third_party_tools\\Login_Information.xlsx")
    sheet = workbook.sheet_by_name("Sheet1") #Read data from Excel sheet named "Sheet1"
    rowcount = sheet.nrows #Get number of rows with data in excel sheet
    colcount = sheet.ncols #Get number of columns with data in each row. Returns highest number
    for curr_row in range(1, rowcount, 1):
        for curr_col in range(1, colcount, 1):
            data = sheet.cell_value(curr_row, curr_col) # Read the data in the current cell
            if(data=='New York Power Authority'):
                username1 =  sheet.cell_value(curr_row, curr_col+1)
                password1 = sheet.cell_value(curr_row, curr_col+2)

    url = 'https://wwwapps.nypa.gov/procurement/Login.aspx'
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    wait = WebDriverWait(driver, 20)
    driver.get(url)
    driver.find_element_by_xpath('//*[@id="accept"]').click()
    username = driver.find_element_by_xpath('//*[@id="email"]')
    password = driver.find_element_by_xpath('//*[@id="password"]')
    username.send_keys(username1)
    password.send_keys(password1)
    driver.find_element_by_xpath('//*[@id="AutoNumber3"]/tbody/tr[3]/td/input').click()

    tidy = BeautifulSoup(driver.page_source, 'html.parser')
    table = tidy.find('table',{"class":"data-form"})
    tr = table.findAll('tr')
    tr = tr[1:]
    for row in tr:
        td = row.findAll('td')
        i=0
        for data in td:
            i+=1
            if(i==1):
                link = "https://wwwapps.nypa.gov/procurement/" + data.find('a').get('href')
            if(i == 2):
                description = data.text.strip()
            if(i==3):
                enddate = data.text.strip()
                enddate = enddate[:10]
            if(i==5):
                category = data.text.strip()
        if(category == 'Computer & Telecom Services' or category == 'Consulting' or category == 'Data Processing'
            or category == 'Engineering Services' or category == 'Information Technology' or category == 'IT testing'):
            fin[description] = link
            results += [[description, link, startdate, enddate]]
            description, link = 0, 0
            startdate, enddate = 'NA', 'NA'
            category = 'NA'
    driver.find_element_by_xpath('//*[@id="AutoNumber1"]/tbody/tr/td/div[1]/ul/li[7]/a').click()
    driver.quit()
    return results
