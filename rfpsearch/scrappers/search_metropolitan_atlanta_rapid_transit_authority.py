# Created by lchhabriya at 10/18/2018
from bs4 import BeautifulSoup
from beautifulscraper import urllib2

def scrap():
    results = []
    fin = dict()
    description, link, startdate, enddate = 0, 0, 'NA', 'NA'
    url = 'https://martabid.marta.net/CurrentOpportunities.aspx'
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')
    div = tidy.findAll('div', {"class": "faqs__item js-expando"})

    for row in div:
        description = row.find('a').text.replace('\r\n','').replace("  ","").replace('\u2003-\u2003','').strip()
        # print(description.index())
        alldiv = row.findAll('div',{"class": "content"})
        for rowlink in alldiv:
            link = rowlink.find('a').get('href')

        if(description != 0):
                fin[description] = link
                results += [[description, link, startdate, enddate]]
                description, link, startdate, enddate= 0, 0, 'NA', 'NA'
    return results



