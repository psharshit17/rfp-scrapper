from bs4 import BeautifulSoup
from beautifulscraper import urllib2


def scrap():
    results = []
    fin = dict()
    description, link = 0, 0
    bidstartdate, bidenddate = 'NA', 'NA'
    category,status = 'NA','NA'
    url = 'https://secure.phila.gov/ECONTRACT/Documents/FrmOpportunityList.aspx '
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')
    div = tidy.find('div',{"class":"divDatagrid"})
    tr = div.findAll('tr')
    tr = tr[1:]
    countsss = 0
    for row in tr:
        i=0
        td = row.findAll('td')
        for data in td:
            # print(data)
            i+=1
            if(i==2):
                description = data.text.strip()
            if(i==4):
                category = data.text
            if(i==6):
                bidstartdate = data.text.strip()
            if(i==7):
                bidenddate = data.text.strip()
            if(i==8):
                status = data.text
            link = url
            if(status == 'Open' ):
                if(category == 'Architect and Engineer Svcs' or category == 'Computer and Information Svcs'):
                    fin[description] = link
                    results += [[description, link, bidstartdate, bidenddate]]
                    description, link = 0, 0
                    bidstartdate, bidenddate = 'NA', 'NA'
                    status = 'NA'
                    category = 'NA'
    return results