from bs4 import BeautifulSoup
from beautifulscraper import urllib2

def scrap():
    results = []
    url = 'https://www.nttamarketplace.org/bso/external/publicBids.sdo'
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')
    spantable = tidy.find('table', {"class": "tabbed-nav"})
    spanclass = spantable.find('span', {"class": "nav-white-simple"})
    bidsstring = spanclass.getText()
    number_of_bids = int(bidsstring[bidsstring.find('(') + 1: bidsstring.find(')')]) # can be used for number of records comparision lchhabriya
    table = tidy.find('table', {"id": "resultsTable"})
    tr = table.find_all('tr')
    tr = tr[1:]
    fin = dict()
    description, link = 0, 0
    bidstartdate, bidenddate = 'NA', 'NA'
    for row in tr:
        td = row.find_all('td')
        i = 0
        for data in td:
            i += 1
            if i == 1:
                link = "https://www.nttamarketplace.org" + data.find('a').get('href')
                description = data.text.strip()

            if i == 4:
                work = data.text.strip()
            if i == 6:
                bidstartdate = data.text.strip()
                bidstartdate = bidstartdate[:10]
        if description != 0:
            description = description + ' - ' + work
            fin[description] = link
            results += [[description, link, bidstartdate, bidenddate ]]
            description, link = 0, 0
            bidstartdate, bidenddate = 'NA', 'NA'
    # print(results)
    # print(results.count())

    return results
