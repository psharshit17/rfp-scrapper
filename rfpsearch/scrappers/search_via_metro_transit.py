# Created by lchhabriya at 10/28/2018

from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
import xlrd
from selenium.webdriver.support.ui import Select
import time


def scrap():
    url = 'https://via.mwdsbe.com/'
    # load new url using selenium
    driver = driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    driver.get(url)
    wait = WebDriverWait(driver, 20)
    results = []
    fin = dict()
    description, link = 0, 0
    startdate, enddate = 'NA', 'NA'
    status = 'NA'
    username = 'NA'
    paswwrod = 'NA'
    status = 'NA'

    workbook = xlrd.open_workbook("third_party_tools\\Login_Information.xlsx")
    sheet = workbook.sheet_by_name("Sheet1")  # Read data from Excel sheet named "Sheet1"
    rowcount = sheet.nrows  # Get number of rows with data in excel sheet
    colcount = sheet.ncols  # Get number of columns with data in each row. Returns highest number

    for curr_row in range(1, rowcount, 1):
        for curr_col in range(1, colcount, 1):
            data = sheet.cell_value(curr_row, curr_col)  # Read the data in the current cell
            if (data == 'Via Metro Transit'):
                username1 = sheet.cell_value(curr_row, curr_col + 1)
                password1 = sheet.cell_value(curr_row, curr_col + 2)
    driver.find_element_by_xpath('//*[@id="container"]/header/div[2]/div/ul/li/a').click()
    username = driver.find_element_by_xpath('// *[ @ id = "txtLogin"]')
    password = driver.find_element_by_xpath('//*[@id="txtPassword"]')

    username.send_keys(username1)
    time.sleep(5)
    password.send_keys(password1)
    time.sleep(5)
    driver.find_element_by_xpath('//*[@id="sign_in"]/div/div/div/input').click()
    try:
        isPresent = driver.find_element_by_xpath('//*[@id="ButtonAssumeExistingSessiononThisComputer"]').size != 0
        if (isPresent):
            driver.find_element_by_xpath('//*[@id="ButtonAssumeExistingSessiononThisComputer"]').click()
    except:
        abc = 1
    executor_url = driver.command_executor._url
    session_id = driver.session_id
    driver2 = webdriver.Remote(command_executor=executor_url, desired_capabilities={})
    driver2.session_id = session_id
    urlnew = 'https://via.mwdsbe.com/Common/Search/OutreachSearch.asp?XID=2942&PID=306&Type=M&Description=Vendor_Search_Outreach&ID=20005682&Parameter=&VendorID=20005682'
    driver2.get(urlnew)
    driver2.find_element_by_xpath('/html/body/form/div[2]/ul/li[4]/a').click()
    select_element = Select(driver.find_element_by_css_selector("body > form > table:nth-child(14) > tbody > tr > td > table > tbody > tr > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > select"))
    select_element.select_by_value('Open')
    driver.find_element_by_xpath('//*[@id="ButtonSearchFirst20Matches"]').click()
    table = driver.find_element_by_xpath('/html/body/form/table[1]/tbody/tr/td/table/tbody/tr/td/table')
    tbody = table.find_element_by_tag_name('tbody')
    tr = tbody.find_elements_by_tag_name('tr')

    tr = tr[2:]
    for row in tr:
        td = row.find_elements_by_tag_name('td')
        i=0
        for data in td:
            i+=1
            if(i==3):
                description = data.text

            if(i==5):
                status = data.text

            if(i==6):
                enddate = data.text.replace('\n','').strip()
            link = 'https://via.mwdsbe.com/?TN=via'
        if(status == 'Open'):
                fin[description] = link
                results += [[description, link, startdate, enddate]]
                description, link = 0, 0
                startdate, enddate = 'NA', 'NA'
                status = 'NA'
    driver.back()
    time.sleep(5)
    driver.back()
    time.sleep(5)
    driver.back()
    driver2.quit()
    driver.quit()
    return results