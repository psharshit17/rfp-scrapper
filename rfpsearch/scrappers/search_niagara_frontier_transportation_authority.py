# Created by lchhabriya at 10/15/2018

import xlrd
from selenium import webdriver  # To launch/initialise a browser
from selenium.webdriver.support.wait import WebDriverWait  # To wait for a page to load.
from bs4 import BeautifulSoup


def scrap():
    # Reading excel sheet for username and password
    workbook = xlrd.open_workbook("third_party_tools\\Login_Information.xlsx")
    sheet = workbook.sheet_by_name("Sheet1")  # Read data from Excel sheet named "Sheet1"
    rowcount = sheet.nrows  # Get number of rows with data in excel sheet
    colcount = sheet.ncols  # Get number of columns with data in each row. Returns highest number

    username1 = 'NA'
    password1 = 'NA'
    startdate, duedate = 'NA', 'NA'
    for curr_row in range(1, rowcount, 1):
        for curr_col in range(1, colcount, 1):
            data = sheet.cell_value(curr_row, curr_col) # Read the data in the current cell
            if( data.strip() == 'Niagara Frontier Transportation Authority'):
                username1 = sheet.cell_value(curr_row, curr_col+1)
                password1 = sheet.cell_value(curr_row, curr_col+2)

    if(username1 == 'NA'):
        results = [['Please check the login information in the Login_Information.xlsx file']]
        return results
    url = 'http://bids.nfta.com/Default.aspx'
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    wait = WebDriverWait(driver, 20)
    driver.get(url)
    driver.find_element_by_xpath('//*[@id="main"]/p[2]/a[1]').click()
    username = driver.find_element_by_xpath('//*[@id="ctl00_ContentPlaceHolder1_Login2_UserName"]')
    password = driver.find_element_by_xpath('//*[@id="ctl00_ContentPlaceHolder1_Login2_Password"]')
    username.send_keys(username1.strip())
    password.send_keys(password1.strip())
    driver.find_element_by_css_selector("#ctl00_ContentPlaceHolder1_Login2_LoginButton").click()  # Login Button

    # Scrapping
    results = []
    fin = dict()
    description, link = 0, 0
    tidy = BeautifulSoup(driver.page_source, 'html.parser')
    table = tidy.find('table',{"id":"ctl00_ContentPlaceHolder1_gvData"})
    tr = table.findAll('tr')
    tr = tr[1:]
    for row in tr:
        i=0
        td = row.findAll('td')
        for data in td:
            if(i==0):
                startdate = data.text
            i+=1
            if(i==3):
                description = data.text
            if(i==4):
                duedate = data.text
            if(i==5):
                link = "http://bids.nfta.com/" + data.find('a').get('href')
        if description != 0:
            fin[description] = link
            results += [[description, link, startdate, duedate]]
            description, link = 0, 0
            startdate, duedate = 'NA', 'NA'
    driver.find_element_by_css_selector('#ctl00_LogoutButton').click()  # Logout button
    driver.quit()
    return results

