# Created by lchhabriya at 10/11/2018
from bs4 import BeautifulSoup
from beautifulscraper import urllib2


def scrap():
    results = []
    fin = dict()
    description, link = 0, 0
    bidstartdate, bidenddate = 'NA', 'NA'
    url = 'https://usage.smud.org/EBSSExt/Default.aspx'
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')
    table = tidy.find('table', {"id": "MasterContent_SolicitationListGridView"})
    # tbody = table.find('tbody')
    tr = table.findAll('tr')
    tr = tr[1:]
    description, link = 0, 0
    bidstartdate, bidenddate = 'NA', 'NA'
    for row in tr:
        i = 0
        td = row.findAll('td')
        for data in td:
            i += 1
            if (i == 1):
                link = "https://usage.smud.org/EBSSExt/" + data.find('a').get('href')
            if( i ==2):
                description = data.text.strip()
            if (i == 4):
                bidenddate = data.text.strip()
                bidenddate = bidenddate[:10]
        if description != 0:
            fin[description] = link
            results += [[description, link, bidstartdate, bidenddate]]
            description, link = 0, 0
            bidstartdate, bidenddate = 'NA', 'NA'
    return results