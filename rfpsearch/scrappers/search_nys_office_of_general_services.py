import xlrd
from selenium import webdriver  # To launch/initialise a browser
from selenium.webdriver.support.wait import WebDriverWait  # To wait for a page to load.
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC


def scrap():
    workbook = xlrd.open_workbook("third_party_tools\\Login_Information.xlsx")
    sheet = workbook.sheet_by_name("Sheet1")  # Read data from Excel sheet named "Sheet1"
    rowcount = sheet.nrows  # Get number of rows with data in excel sheet
    colcount = sheet.ncols  # Get number of columns with data in each row. Returns highest number

    results = []
    fin = dict()
    description, link = 0, 0
    startdate, enddate = 'NA', 'NA'
    username = 'NA'
    paswwrod = 'NA'

    for curr_row in range(1, rowcount, 1):
        for curr_col in range(1, colcount, 1):
            data = sheet.cell_value(curr_row, curr_col)  # Read the data in the current cell
            if (data == 'NYS Office of General Services'):
                username1 = sheet.cell_value(curr_row, curr_col + 1)
                password1 = sheet.cell_value(curr_row, curr_col + 2)

    url = 'https://online.ogs.ny.gov/DNC/DNCPortal/ECPLogin.aspx'
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    wait = WebDriverWait(driver, 20)
    driver.get(url)

    username = driver.find_element_by_xpath('//*[@id="Login1_UserName"]')
    password = driver.find_element_by_xpath('//*[@id="Login1_Password"]')

    username.send_keys(username1)
    password.send_keys(password1)

    driver.find_element_by_css_selector("#Login1_LoginButton").click()
    driver.find_element_by_css_selector("#ctl00_tvECPn1").click()
    driver.find_element_by_css_selector("#ctl00_ContentPlaceHolder1_btnAll").click()

    tr = driver.find_elements_by_xpath('//*[@id="ctl00_ContentPlaceHolder1_dgProjects"]/tbody/tr')
    tr = tr[1:]
    i = 1
    for row in tr:
        i += 1
        wait.until(EC.presence_of_element_located(
            (By.XPATH, '//*[@id="ctl00_ContentPlaceHolder1_dgProjects"]/tbody/tr[' + str(i) + ']/td[1]/a')))
        id = driver.find_element_by_xpath(
            '//*[@id="ctl00_ContentPlaceHolder1_dgProjects"]/tbody/tr[' + str(i) + ']/td[1]/a').text
        link = 'https://online.ogs.ny.gov/DNC/contractorConsultant/esb/ESBPlansAvailable.asp?Project=' + id
        driver.switch_to.window(driver.window_handles[0])
        description = driver.find_element_by_xpath(
            '//*[@id="ctl00_ContentPlaceHolder1_dgProjects"]/tbody/tr[' + str(i) + ']/td[3]').text
        enddate = driver.find_element_by_xpath(
            '//*[@id="ctl00_ContentPlaceHolder1_dgProjects"]/tbody/tr[' + str(i) + ']/td[4]').text
        if (description != 0):
            fin[description] = link
            results += [[description, link, startdate, enddate]]
            description, link = 0, 0
            startdate, enddate = 'NA', 'NA'
    driver.find_element_by_xpath('//*[@id="ctl00_tvECPn6"]').click()
    driver.quit()
    return results
