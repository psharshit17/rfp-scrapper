from bs4 import BeautifulSoup
from beautifulscraper import urllib2

def scrap():
    results = []
    url = 'http://goldengate.org/contracts/index.php'
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')
    div = tidy.find('div', {"class": "contract-table"})
    tr = div.findAll('tr')
    tr3 = tr[3:]
    for row in tr3:
        td = row.find_all('td')
        i = 0
        for data in td:
            i += 1
            if i == 2:
                description = data.text
                link = "http://goldengate.org/contracts/" + row.find('a').get('href')
                results.append([description, link])
    return results
