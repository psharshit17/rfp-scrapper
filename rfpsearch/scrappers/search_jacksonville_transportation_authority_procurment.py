from bs4 import BeautifulSoup
from beautifulscraper import urllib2

def scrap():
    results = []
    url = 'https://procurement.jtafla.com/solicitations'
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')
    table = tidy.find('table', {"class": "table table-striped"})
    tbody = table.find('tbody')
    tr = tbody.findAll('tr')
    fin = dict()
    description, link = 0, 0
    for row in tr:
        td = row.find_all('td')
        i = 0
        for data in td:
            i += 1
            if i == 1:
                link = "https://procurement.jtafla.com" + data.find('a').get('href')
            elif i == 2:
                description = data.text
        if description != 0:
            fin[description] = link
            results += [[description, link]]
            description, link = 0, 0

    return results