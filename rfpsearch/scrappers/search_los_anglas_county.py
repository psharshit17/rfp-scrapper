from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait


def scrap():
    url = 'http://camisvr.co.la.ca.us/lacobids/BidLookUp/BLView.asp?sType=&sText=p'
    # load new url using selenium
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    driver.get(url)
    wait = WebDriverWait(driver, 20)
    results = []
    fin = dict()
    description, link = 0, 0
    startdate,enddate = 'NA','NA'
    status = 'NA'
    pagenumberstrip = driver.find_element_by_css_selector('#BLU > table:nth-child(9) > tbody > tr > td > p:nth-child(1) > b')
    pages = int(pagenumberstrip.text.split('of')[1])
    # pagenumberstriptext = pagenumberstrip.text
    for x in range(0,pages):
        tbody = driver.find_element_by_xpath('//*[@id="BLU"]/table[1]/tbody')
        tr = tbody.find_elements_by_tag_name('tr')
        tr = tr[1:]

        for row in tr:
            i = 0
            td = row.find_elements_by_tag_name('td')
            for data in td:
                i+=1
                if(i==2):
                    description = data.text.strip()
                if(i==5):
                    enddate = data.text.strip()
                link = 'http://camisvr.co.la.ca.us/lacobids/BidLookUp/BidLookUpFrm.asp'
            if(description!=0):
                fin[description] = link
                results += [[description, link, startdate, enddate]]
                description, link = 0, 0
                startdate, enddate = 'NA', 'NA'
        driver.find_element_by_xpath('//*[@id="BLU"]/p/table/tbody/tr/td[3]/a').click()
    driver.quit()
    return results