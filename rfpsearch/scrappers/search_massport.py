# Created by lchhabriya at 11/14/2018
from bs4 import BeautifulSoup
from beautifulscraper import urllib2


def scrap():
    try:
        results = []
        fin = dict()
        description, link = 0, 0
        bidstartdate, bidenddate = 'NA', 'NA'
        url = 'http://www.massport.com/massport/business/bids-opportunities/current-opportunities/'
        page = urllib2.urlopen(url)
        tidy = BeautifulSoup(page, 'html.parser')
        table = tidy.find('table', {"class": "solicitations-table"})
        for script in table.find('thead'):
            script.extract()
        tr = table.findAll('tr')
        tr = tr[1:]
        for row in tr:
             td = row.findAll('td')
             i = 0
             for data in td:
                i += 1
                if (i == 1):
                    description = data.text.strip()
                    link = "http://www.massport.com" + data.find('a').get('href')
                if (i == 2):
                    bidenddate = data.text.strip()
             if description != 0:
                fin[description] = link
                results += [[description, link, bidstartdate, bidenddate]]
                description, link = 0, 0
                bidstartdate, bidenddate = 'NA', 'NA'
        return results
    except:
        if(len(results)==0):
            results = [[
                           "No RFP to show, either zero RFP's or website error, Please check the original website or contact system administrator"]]
        return results
