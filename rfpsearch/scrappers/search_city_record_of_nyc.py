# Created by lchhabriya at 10/28/2018

from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.ui import Select


def scrap():
    url = 'https://a856-cityrecord.nyc.gov/Section'
    # load new url using selenium
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    wait = WebDriverWait(driver, 20)
    driver.get(url)
    driver.find_element_by_xpath('/html/body/div[3]/div/div[1]/div/div/div[2]/div[2]/div/a[2]').click()
    driver.find_element_by_xpath('//*[@id="AdvancedSearchForm"]/button').click()
    select_element = Select(driver.find_element_by_xpath('//*[@id="ddlCategory"]'))
    select_element.select_by_value('2')
    driver.find_element_by_xpath('//*[@id="AdvancedSubmitButton"]').click()
    results = []
    fin = dict()
    description, link = 0, 0
    bidstartdate, bidenddate = 'NA', 'NA'
    while True:
        div = driver.find_element_by_xpath('//*[@id="searchResults"]/div/div[2]')
        divs = div.find_elements_by_class_name("notice-container")
        i=0
        for row in divs:
            link = row.find_element_by_tag_name('a').get_attribute('href')
            header = row.find_element_by_tag_name('a')
            description = header.text
            print(description)
            if description != 0:
                    fin[description] = link
                    results += [[description, link, bidstartdate, bidenddate]]
                    description, link = 0, 0
                    bidstartdate, bidenddate = 'NA', 'NA'
        try:
            driver.find_element_by_xpath('//*[@id="searchResults"]/div/div[2]/div[1]/ul/li[6]/a').click()
        except:
            break
    # For second category
    select_element = Select(driver.find_element_by_xpath('//*[@id="ddlCategory"]'))
    select_element.select_by_value('7')
    driver.find_element_by_xpath('//*[@id="AdvancedSubmitButton"]').click()
    while True:
        div = driver.find_element_by_xpath('//*[@id="searchResults"]/div/div[2]')
        divs = div.find_elements_by_class_name("notice-container")
        i=0
        for row in divs:
            link = row.find_element_by_tag_name('a').get_attribute('href')
            header = row.find_element_by_tag_name('a')
            description = header.text
            if description != 0:
                    fin[description] = link
                    results += [[description, link, bidstartdate, bidenddate]]
                    description, link = 0, 0
                    bidstartdate, bidenddate = 'NA', 'NA'
        try:
            driver.find_element_by_xpath('//*[@id="searchResults"]/div/div[2]/div[12]/ul/li[6]/a').click()
        except:
            break
    driver.quit()
    return results