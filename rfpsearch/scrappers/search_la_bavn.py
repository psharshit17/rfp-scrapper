# Created by lchhabriya at 10/11/2018

from bs4 import BeautifulSoup
from beautifulscraper import urllib2

def scrap():
    results = []
    fin = dict()
    description, link = 0, 0
    bidstartdate, bidenddate = 'NA', 'NA'
    url = 'https://www.labavn.org/'
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')
    table = tidy.find('table', {"class": "table table-hover"})
    tbody = table.find('tbody')
    tr = tbody.findAll('tr')
    description, link = 0, 0
    bidstartdate, bidenddate = 'NA', 'NA'
    status = 'NA'
    for row in tr:
        i = 0
        td = row.findAll('td')
        for data in td:
            i += 1
            if (i == 2):
                link = "https://www.labavn.org/" + data.find('a').get('href')
                description = data.find('a').text.replace("\r\n","").strip()
            if (i == 5):
                bidenddate = data.text.strip()
            if(i ==6 ):
                status = data.text
        if status == 'Open':
            fin[description] = link
            results += [[description, link, bidstartdate, bidenddate]]
            description, link = 0, 0
            bidstartdate, bidenddate = 'NA', 'NA'
            status = 'NA'
    return results
