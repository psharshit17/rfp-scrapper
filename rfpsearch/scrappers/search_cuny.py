# Created by lchhabriya at 10/10/2018

from bs4 import BeautifulSoup
from beautifulscraper import urllib2


def scrap():
    results = []
    fin = dict()
    description, link = 0, 0
    bidstartdate, bidenddate = 'NA', 'NA'
    url = 'http://web.cuny.edu/administration/cb/construction-solicitations.html'
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')
    div = tidy.find('div',{"id":"body"})
    ul = div.find('ul')
    li = ul.findAll('li')
    for row in li:
        description = row.text
        link = "http://web.cuny.edu/administration/cb/" + row.find('a').get('href')
        if description != 0:
            fin[description] = link
            results += [[description, link, bidstartdate, bidenddate]]
            description, link = 0, 0
            bidstartdate, bidenddate = 'NA', 'NA'
    return results

