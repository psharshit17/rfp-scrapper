from bs4 import BeautifulSoup
from beautifulscraper import urllib2


def scrap():
    results = []
    url = 'https://www.ridemetroapp.org/procurement/solicitations.aspx'
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')
    table = tidy.find('table', {"id": "ctl00_ContentPlaceHolder1_table_solicitations"})
    tr = table.findAll('tr')
    tr = tr[1:]

    fin = dict()
    description, link = 0, 0
    bidstartdate, bidduedate = 'NA', 'NA'
    for row in tr:
        td = row.find_all('td')
        i = 0
        for data in td:
            i += 1
            if i == 1:
                solicitationnumber = data.text.strip()
                link = "https://www.ridemetroapp.org/procurement/" + data.find('a').get('href')
            if i == 2:
                description = data.text.strip()
                description.replace('\Xa0', ' ')
            if i == 3:
                bidduedate = data.text.strip()
                bidduedate = bidduedate[:10]
        #           if i == 6:
        #                link = "http://www.saws.org/business_center/procbids/" + data.find('a').get('href')
        if description != 0:
            description = solicitationnumber + ' - ' + description
            fin[description] = link
            results += [[description, link, bidstartdate, bidduedate]]
            description, link = 0, 0
            bidstartdate, bidduedate = 'NA', 'NA'
    return results