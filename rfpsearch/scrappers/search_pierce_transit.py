from bs4 import BeautifulSoup
from beautifulscraper import urllib2

def scrap():
    results = []
    url = 'https://www.piercetransit.org/open-procurements/'
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')
    div = tidy.find('div', {"class": "roundbox"})
    tablebody = div.find('tbody')
    tr = tablebody.findAll('tr')
    fin = dict()
    description, link = 0, 0
    bidstartdate, bidduedate = 'NA', 'NA'
    for row in tr:
        td = row.find_all('td')
        i = 0
        for data in td:
            i += 1
            if i == 1:
                description = data.text.strip()
            if i == 2:
                bidstartdate = data.text.strip()
            if i == 3:
                bidduedate = data.text.strip()
            if i == 4:
                link = "https://www.piercetransit.org" + data.find('a').get('href')
        if description != 0:
            # description = solicitationnumber + ' - ' + description
            fin[description] = link
            results += [[description, link, bidstartdate, bidduedate]]
            description, link = 0, 0
            bidstartdate, bidduedate = 'NA', 'NA'
    return results