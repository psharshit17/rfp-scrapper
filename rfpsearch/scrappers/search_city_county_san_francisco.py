from selenium import webdriver  # To launch/initialise a browser
from selenium.webdriver.support.wait import WebDriverWait  # To wait for a page to load.
from bs4 import BeautifulSoup  # scrapping
from selenium.common.exceptions import NoSuchElementException  # Exception Handling


def scrap():
    results = []
    url = 'http://mission.sfgov.org/OCABidPublication/ReviewBids.aspx'
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    wait = WebDriverWait(driver, 20)
    driver.get(url)

    # for looping the whole website
    while True:
        tidy = BeautifulSoup(driver.page_source, 'html.parser')
        div = tidy.find('div', {"id": "_ctl0_cp_BODY_CONTENT_ReviewBidsBody_PANEL_VIEW_ALL_DOCUMENTS"})
        tr = div.find_all('tr')
        tr2 = tr[2:]
        fin = dict()
        description, link = 0, 0

        # for looping all the RFPs from the webpage
        for row in tr2:
            # td = row.find_all('td')
            td = row.findAll('td')
            # for data in td:
            i = 0
            for tds in td:   # For looping among all the td's
                i += 1
                if i == 1:
                    link = tds.find('a')
                    if link != None:
                        if not link.get('href').startswith('javascript'):
                            linknew = "http://mission.sfgov.org" + link.get('href')
                elif i == 2:
                    description = tds.text

            if description != 0 and linknew!=0:
                fin[description] = linknew
                results += [[description, linknew]]
                description, linknew = 0, 0
        try:
            # This will load a next page
            driver.find_element_by_css_selector(
                '#_ctl0_cp_BODY_CONTENT_ReviewBidsBody_DG_ALL_DOCUMENTS > tbody > tr.table-footer > td > a:nth-child(2)').click()
        except NoSuchElementException:
            # When reached the last page
            break
    driver.quit()  # Closes the window opened by the web driver
    return results


