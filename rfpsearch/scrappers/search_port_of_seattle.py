# Created by lchhabriya at 11/12/2018

from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
import time


def scrap():
    try:
        url = 'https://hosting.portseattle.org/prms/Solicitations'
        # load new url using selenium
        driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
        driver.get(url)
        wait = WebDriverWait(driver, 20)
        results = []
        fin = dict()
        description, link = 0, 0
        startdate, enddate = 'NA', 'NA'
        status = 'NA'
        link = url
        pagenumberstrip = driver.find_element_by_css_selector('#masterDataTable_info')
        pages = (pagenumberstrip.text)
        element = driver.find_element_by_xpath('//*[@id="masterDataTable_next"]')
        while True:
            time.sleep(1)
            # wait.until(EC.presence_of_element_located(By.XPATH('//*[@id="masterDataTable_next"]')))
            div = driver.find_element_by_xpath('//*[@id="masterDataTable"]/tbody')
            tr = div.find_elements_by_tag_name('tr')
            div = 0
            for row in tr:
                # print('2')
                i = 0
                td = row.find_elements_by_tag_name('td')
                for data in td:
                    i += 1
                    if (i == 1):
                        description = data.text
                    if (i == 2):
                        enddate = data.text
                    if (i == 4):
                        status = data.text
                if (status == 'Open'):
                    fin[description] = link
                    results += [[description, link, startdate, enddate]]
                    description = 0
                    startdate, enddate = 'NA', 'NA'
                    status = 'NA'
            if (element.is_enabled()):
                element = element.click()
            else:
                break
    except:
        driver.quit()
        return results


