# Created by lchhabriya at 10/17/2018

from bs4 import BeautifulSoup
from beautifulscraper import urllib2


def scrap():
    results = []
    url = 'http://app.ocp.dc.gov/RUI/information/scf/SolNumRespond.asp'
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')
    table = tidy.find('table')
    tr = table.findAll('tr')
    tr = tr[2:]
    fin = dict()
    description = 0
    link = 0
    bidstartdate, bidduedate = 'NA', 'NA'
    status = 'NA'
    for row in tr:
        td = row.find_all('td')
        i = 0
        for data in td:
            i += 1
            if i == 1:
                abc = data.find("a")
                if(abc != None):
                 link = "http://app.ocp.dc.gov/RUI/information/scf/" + abc.get('href')
            if i == 2:
                description = data.text.strip()
            if i == 3:
                bidduedate = data.text.strip()
            if i == 5:
                bidstartdate = data.text.strip()
            if(i == 8):
                status = data.text.strip()

        if status != 'Closed':
            fin[description] = link
            results += [[description, link, bidstartdate, bidduedate]]
            description, link = 0, 0
            bidstartdate, bidduedate = 'NA', 'NA'
            status = 'NA'
    return results
