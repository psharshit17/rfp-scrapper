from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait


def scrap():
    url = 'http://www.fultoncountyga.gov/county/bidss/index.php?function=search&table_name=bids&where_clause=BidStatus=1&page=0'
    # load new url using selenium
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    driver.get(url)
    wait = WebDriverWait(driver, 20)
    results = []
    fin = dict()
    description, link = 0, 0
    startdate, enddate = 'NA', 'NA'
    status = 'NA'
    pagestrip = driver.find_element_by_xpath('/html/body/table/tbody/tr/td/font[3]')
    pages = int(pagestrip.text.split('of')[1])
    for x in range(0,pages):
        url = 'http://www.fultoncountyga.gov/county/bidss/index.php?function=search&table_name=bids&where_clause=BidStatus=1&page=' + str(x)
        driver.get(url)
        tbody = driver.find_element_by_xpath('/html/body/table/tbody/tr/td/table[1]/tbody')
        tr = tbody.find_elements_by_tag_name('tr')
        tr = tr[1:]
        for row in tr:
            td = row.find_elements_by_tag_name('td')
            i = 0
            for data in td:
                i += 1
                if (i == 1):
                    link = data.find_element_by_tag_name('a').get_attribute('href')
                if (i == 2):
                    startdate = data.text
                if (i == 3):
                    enddate = data.text
                if (i == 4):
                    enddate = enddate + " " + data.text
                if (i == 5):
                    description = data.text.replace('\n', ' ')
                if (i == 11):
                    status = data.text
            if (status == 'Open'):
                fin[description] = link
                results += [[description, link, startdate, enddate]]
                description, link = 0, 0
                startdate, enddate = 'NA', 'NA'
                status = 'NA'
    driver.quit()
    return results
