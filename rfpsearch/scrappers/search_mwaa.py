# Created by lchhabriya at 10/28/2018

from bs4 import BeautifulSoup
from beautifulscraper import urllib2


def scrap():
    results = []
    fin = dict()
    startdate,enddate = 'NA','NA'
    url = 'http://www.mwaa.com/business/current-contracting-opportunities'
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')
    div = tidy.find('div',{"class":"tab-content"})
    servicesdiv = div.find('div',{"id":"services"})
    servicesp = servicesdiv.findAll('p')

    servicesp = servicesp
    for rowp in servicesp:
        i=0
        serviceb = rowp.findAll('b')
        link = "http://www.mwaa.com" + rowp.find('a').get('href')
        for servicedata in serviceb:
            #
            i+=1
            if(i==1):
                description = servicedata.text.replace('NEW','').replace('UPDATED','').strip()

        if (description !=0):
            fin[description] = link
            results += [[description, link, startdate, enddate]]
            description, link = 0, 0
            startdate, enddate = 'NA', 'NA'
    servicesdiv = div.find('div',{"id":"db"})
    servicesp = servicesdiv.findAll('p')
    servicesp = servicesp
    for rowp in servicesp:
        i=0
        serviceb = rowp.findAll('b')
        if(rowp.find('a')):
            link = "http://www.mwaa.com" + rowp.find('a').get('href')
            for servicedata in serviceb:
                #
                i+=1
                if(i==1):
                    description = servicedata.text.replace('NEW','').replace('UPDATED','').strip()

            if (description !=0):
                fin[description] = link
                results += [[description, link, startdate, enddate]]
                description, link = 0, 0
                startdate, enddate = 'NA', 'NA'
    servicesdiv = div.find('div',{"id":"ae"})
    servicesp = servicesdiv.findAll('p')
    servicesp = servicesp
    for rowp in servicesp:
        i=0
        serviceb = rowp.findAll('b')
        if(rowp.find('a')):
            link = "http://www.mwaa.com" + rowp.find('a').get('href')
            for servicedata in serviceb:
                #
                i+=1
                if(i==1):
                    description = servicedata.text.replace('NEW','').replace('UPDATED','').strip()

            if (description !=0):
                fin[description] = link
                results += [[description, link, startdate, enddate]]
                description, link = 0, 0
                startdate, enddate = 'NA', 'NA'
    return results