from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select


def scrap():
    url = 'https://www.commbuys.com/bso/external/publicBids.sdo'
    # load new url using selenium
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    driver.get(url)
    wait = WebDriverWait(driver, 20)
    results = []
    fin = dict()
    description, link = 0, 0
    startdate,enddate = 'NA','NA'
    status = 'NA'

    select_element = Select(driver.find_element_by_css_selector("#cat"))
    select_element.select_by_value('43')
    driver.find_element_by_css_selector('body > form > table.table-01 > tbody > tr:nth-child(2) > td > table > tbody > tr > td > input').click()
    if(driver.find_element_by_xpath('/html/body/form/table[4]/tbody/tr[4]/td/table/tbody/tr[2]/td')):
        pageNums = driver.find_element_by_xpath('/html/body/form/table[4]/tbody/tr[4]/td/table/tbody/tr[2]/td')
        pageNumsstr = pageNums.text
        if(pageNumsstr.isdigit()):
            totalPagess = pageNumsstr.strip().split(" ")
        else:
            totalPagess = 1

    table = driver.find_element_by_xpath('//*[@id="resultsTable"]')
    tbody = table.find_element_by_tag_name('tbody')
    tr = tbody.find_elements_by_tag_name('tr')
    for pageNumIT in range(0,totalPagess):
        if int(pageNumIT) > 1:
            wait.until(EC.presence_of_element_located((By.LINK_TEXT, pageNums))).click()
            # table = driver.find_element_by_xpath('//*[@id="resultsTable"]/tbody/tr')
            # tbody = table.find_element_by_tag_name('tbody')
        tr = driver.find_elements_by_xpath('//*[@id="resultsTable"]/tbody/tr')
        for row in tr:
            i=0
            td = row.find_elements_by_tag_name('td')
            for data in td:
                i+=1
                if(i==1):
                    link = data.find_element_by_tag_name('a').get_attribute('href')
                if(i==4):
                    description = data.text.strip()
                if(i==6):
                    startdate = data.text.strip()
            if(description != 0):
                fin[description] = link
                results += [[description, link, startdate, enddate]]
                description, link = 0, 0
                startdate, enddate = 'NA', 'NA'


    # For second category
    select_element = Select(driver.find_element_by_css_selector("#cat"))
    select_element.select_by_value('81')
    driver.find_element_by_css_selector('body > form > table.table-01 > tbody > tr:nth-child(2) > td > table > tbody > tr > td > input').click()

    pages = driver.find_element_by_xpath('/html/body/form/table[4]/tbody/tr[4]/td/table/tbody/tr[2]/td')
    totalPages = pages.text.strip().split(" ")
    for pageNum in totalPages:

        if int(pageNum) > 1:
            wait.until(EC.presence_of_element_located((By.LINK_TEXT, pageNum))).click()
            # table = driver.find_element_by_xpath('//*[@id="resultsTable"]/tbody/tr')
            # tbody = table.find_element_by_tag_name('tbody')
        tr = driver.find_elements_by_xpath('//*[@id="resultsTable"]/tbody/tr')

        for row in tr:

            i=0
            td = row.find_elements_by_tag_name('td')
            for data in td:
                i+=1
                if(i==1):
                    link = data.find_element_by_tag_name('a').get_attribute('href')
                if(i==4):
                    description = data.text.strip()
                if(i==6):
                    startdate = data.text.strip()
            if(description != 0):
                fin[description] = link
                results += [[description, link, startdate, enddate]]
                description, link = 0, 0
                startdate, enddate = 'NA', 'NA'
    driver.quit()
    return results

