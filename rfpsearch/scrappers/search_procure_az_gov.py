# Created by lchhabriya at 10/11/2018

from beautifulscraper import urllib2
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup

def scrap():
    results = []
    url = 'https://procure.az.gov/bso/external/publicBids.sdo'
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')

    # load new url using selenium
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    driver.set_window_size(1024, 768)
    wait = WebDriverWait(driver, 20)
    driver.get(url)

    fin = dict()
    bidnumber, description, link, startdate, enddate = 0, 0, 0, 'NA', 'NA'

    tr = driver.find_elements_by_xpath('//*[@id="resultsTable"]/tbody/tr')
    for row in tr:
        i = 0
        tds = row.find_elements_by_tag_name('td')
        for data in tds:
            if i == 0:
                bidnumber = data.text
                link = data.find_element_by_tag_name('a').get_attribute("href")
            i += 1
            if (i == 4):
                description = data.text
            if (i == 6):
                startdate = data.text.strip()
                startdate = startdate[:10]
        if description != 0 and link != 0:
            if not link.startswith('javascript'):
                description = bidnumber + ' - ' + description
                fin[description] = link
                results += [[description, link, startdate, enddate]]
                bidnumber, description, link, startdate, enddate = 0, 0, 0, 'NA', 'NA'
    driver.quit()
    return results