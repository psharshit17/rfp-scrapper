# Created by lchhabriya at 11/12/2018

import xlrd
from selenium import webdriver  # To launch/initialise a browser
from selenium.webdriver.support.wait import WebDriverWait  # To wait for a page to load.


def scrap():
    results = []
    fin = dict()
    description, link = 0, 0
    startdate, enddate = 'NA', 'NA'
    username = 'NA'
    paswwrod = 'NA'

    workbook = xlrd.open_workbook("third_party_tools\\Login_Information.xlsx")
    sheet = workbook.sheet_by_name("Sheet1")  # Read data from Excel sheet named "Sheet1"
    rowcount = sheet.nrows  # Get number of rows with data in excel sheet
    colcount = sheet.ncols  # Get number of columns with data in each row. Returns highest number

    for curr_row in range(1, rowcount, 1):
        for curr_col in range(1, colcount, 1):
            data = sheet.cell_value(curr_row, curr_col)  # Read the data in the current cell
            if (data == 'WSSC One-Source'):
                username1 = sheet.cell_value(curr_row, curr_col + 1)
                password1 = sheet.cell_value(curr_row, curr_col + 2)

    url = 'https://onesource.wsscwater.com/OA_HTML/RF.jsp?function_id=1046014&resp_id=51185&resp_appl_id=396&security_group_id=0&lang_code=US&params=g5HefZDGsGSCj-9y5mFJjOOmGikt1h6IJDSPYc8R0rE&oas=kurwL32r_QOJJEepWbtebg..'
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    wait = WebDriverWait(driver, 20)
    driver.get(url)

    username = driver.find_element_by_xpath('//*[@id="unamebean"]')
    password = driver.find_element_by_xpath('//*[@id="pwdbean"]')

    username.send_keys(username1)
    password.send_keys(password1)

    driver.find_element_by_css_selector("#SubmitButton").click()
    tbody = driver.find_element_by_xpath('//*[@id="WSSCActiveSolicitiationVO1"]/table[2]/tbody')
    tr = tbody.find_elements_by_tag_name('tr')
    tr = tr[1:]
    link = url
    for row in tr:
        i=0
        td = row.find_elements_by_tag_name('td')
        for data in td:
            i+=1
            # if(i==1):
            #     link = data.find_element_by_tag_name('a').get_attribute('href')
            if(i==4):
                description = data.text.strip()
            if(i==5):
                enddate = data.text.strip()

        if (description != 0):
            fin[description] = link
            results += [[description, link, startdate, enddate]]
            description = 0
            startdate, enddate = 'NA', 'NA'
    driver.find_element_by_xpath('//*[@id="ActiveSolicitationsRN"]/table[1]/tbody/tr[2]/td/table/tbody/tr[2]/td[2]/table/tbody/tr/td[3]/a').click()
    driver.quit()
    return results
