from bs4 import BeautifulSoup
from beautifulscraper import urllib2


def scrap():
    results = []
    url = 'http://www.vta.org/about-us/procurement'
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')
    table = tidy.find('table', {"id": "j_id0:SiteTemplateEmpty:j_id109_0:j_idd0:j_idd2:j_idd4"})
    tablebody = table.find('tbody')
    fin = dict()
    description, link = 0, 0

    for row in tablebody:
        link = "http://www.vta.org" + row.find('a').get('href')
        description = row.find('a').getText()
        if description != 0:
            fin[description] = link
            results += [[description, link]]
    return results

