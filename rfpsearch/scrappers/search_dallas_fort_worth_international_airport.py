# Created by lchhabriya at 10/26/2018
from bs4 import BeautifulSoup
from beautifulscraper import urllib2


def scrap():
    results = []
    url = 'https://www.dfwairport.com/business/solicitations/index.php'
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')
    div = tidy.find('table',{"style":"display:table;"})
    table = div.find('table',{"width":"100%"})
    tr = table.findAll('tr')
    fin = dict()
    description, link = 0, 0
    bidstartdate, bidenddate = 'NA', 'NA'
    for row in tr:
        if(row.find('div')):
            div = row.find('div').text
            if('!' not in div):
                description = div
        # td = row.findAll('td')
        # i=0
        # for data in td:
        #     i+=1
        #     if(i==5 and data.text != ''):
        #         bidenddate = data.text.strip()
        link = url
        if(description!=0):
            fin[description] = link
            results += [[description, link, bidstartdate, bidenddate]]
            description, link = 0, 0
            bidstartdate, bidenddate = 'NA', 'NA'
    return results




