# Created by lchhabriya at 10/19/2018

from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

def scrap():
    url = 'http://www.fultonschools.org/en/divisions/finserv/contract'
    # load new url using selenium
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    wait = WebDriverWait(driver, 20)
    driver.get(url)

    wait.until(EC.presence_of_element_located(
        (By.XPATH, '//*[@id="{923C0780-EC1C-44AF-91D2-6B9079BB7953}-{D4A9A5CC-8C75-4D3C-BF83-650E30304192}"]/tbody/tr')))

    tr = driver.find_elements_by_xpath(
        '//*[@id="{923C0780-EC1C-44AF-91D2-6B9079BB7953}-{D4A9A5CC-8C75-4D3C-BF83-650E30304192}"]/tbody/tr')

    results = []
    fin = dict()
    description, link = 0, 0
    startdate, enddate = 'NA', 'NA'
    for row in tr:
        tds = row.find_elements_by_tag_name('td')
        description = tds[2].text
        enddate = tds[3].text
        link = row.find_element_by_tag_name('a').get_attribute("href")
        if (description != 0):
            fin[description] = link
            results += [[description, link, startdate, enddate]]
            description, link = 0, 0
            startdate, enddate = 'NA', 'NA'
    driver.quit()
    return results
