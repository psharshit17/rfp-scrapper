# Created by lchhabriya at 10/11/2018

from bs4 import BeautifulSoup
from beautifulscraper import urllib2


def scrap():
    results = []
    url = 'https://www.njta.com/doing-business/current-solicitations'
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')
    div = tidy.find('div',{"class":"wrap-table"})
    divcontent = div.find('div',{"class":"t-content"})
    divrow = divcontent.findAll('div',{"class":"t-row"})

    fin = dict()
    bidnumber, description, link, startdate, enddate, status = 0, 0, 0, 'NA', 'NA', 'NA'
    for row in divrow:
        i = 0
        p = row.findAll('p')
        for data in p:
            i+=1
            if(i == 1):
                enddate = data.text
            if(i==2):
                status = data.find('span')['class']
            if(i==4):
                link = "https://www.njta.com" + data.find('a').get('href')
                description = data.text
        if('status-inactive' not in status):
            fin[description] = link
            results += [[description, link, startdate, enddate]]
            bidnumber, description, link, startdate, enddate, status = 0, 0, 0, 'NA', 'NA', 'NA'
    return results