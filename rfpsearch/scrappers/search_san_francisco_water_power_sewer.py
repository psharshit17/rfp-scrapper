from selenium import webdriver  # To launch/initialise a browser
from selenium.webdriver.support.wait import WebDriverWait  # To wait for a page to load.
from bs4 import BeautifulSoup  # scrapping
from selenium.common.exceptions import NoSuchElementException  # Exception Handling


def scrap():
    results = []
    url = "https://sfwater.org/bids/bidList.aspx?bidtype=1"
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    wait = WebDriverWait(driver, 20)
    driver.get(url)

    # To find number of pages
    forpagenumber = BeautifulSoup(driver.page_source, 'html.parser')
    pagenumberdetails = forpagenumber.find('span', {"id": "ctl00_ContentPlaceHolder1_DataPager1"})
    pagenumbersection = pagenumberdetails.findAll('li')
    pagenumberstring = pagenumbersection[3].getText()
    slicingpagenumber = pagenumberstring.find('of') + 2
    pagenumber = int(pagenumberstring[slicingpagenumber:].replace(" ", '')) // 10
    remainder = int(pagenumberstring[slicingpagenumber:].replace(" ", '')) % 10
    if(remainder>0):
        pagenumber = pagenumber+1
    print(pagenumber)
    # loop of web site
    for i in range(1, pagenumber):
        tidy = BeautifulSoup(driver.page_source, 'html.parser')
        tr = tidy.find_all('ul', {"id": "table"})
        fin = dict()
        description, link = 0, 0
        for row in tr:
            link = "https://sfwater.org/bids/" + row.find('a').get('href')
            description = row.find('span', {"class": "col2"}).contents[0]
            if description != 0:
                fin[description] = link
                results += [[description, link]]
                description, linknew = 0, 0
        try:
            # This will load a next page
            driver.find_element_by_css_selector('#ctl00_ContentPlaceHolder1_DataPager1_ctl00_NextButton').click()
        except NoSuchElementException:
            break  # When reached the last page
    driver.quit()
    # print(results)
    # print(pagenumber)
    # results = []
    # url = "https://sfwater.org/bids/bidList.aspx?bidtype=1"
    # page = urllib2.urlopen(url)
    # tidy = BeautifulSoup(page, 'html.parser')
    # pagenumberdetails = tidy.find('span', {"id": "ctl00_ContentPlaceHolder1_DataPager1"})
    # pagenumbersection = pagenumberdetails.findAll('li')
    # pagenumberstring = pagenumbersection[3].getText()
    # slicingpagenumber = pagenumberstring.find('of') + 2
    # pagenumber = int(pagenumberstring[slicingpagenumber:].replace(" ", ''))
    # tr = tidy.find_all('ul', {"id": "table"})
    # fin = dict()
    # description, link = 0, 0
    # for row in tr:
    #     link = "https://sfwater.org/bids/" + row.find('a').get('href')
    #     description = row.find('span', {"class": "col2"}).contents[0]
    #     if description != 0:
    #         fin[description] = link
    #         results += [[description, link]]
    #
    # # print(results)
    return results
