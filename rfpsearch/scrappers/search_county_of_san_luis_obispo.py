from bs4 import BeautifulSoup
from beautifulscraper import urllib2
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from urllib.request import Request, urlopen


def scrap():
    req = Request('http://www.slocounty.ca.gov/Departments/Central-Services/Purchasing-Services/Services/Bid-RFP-Opportunities.aspx',headers={'User-Agent': 'Mozilla/5.0'})
    webpage = urlopen(req).read()
    tidy = BeautifulSoup(webpage, 'html.parser')

    # find iframe and get new url
    frame = tidy.find('iframe', {'class': 'embed-responsive-item'})
    url = frame.attrs['src']

    # load new url using selenium
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    wait = WebDriverWait(driver, 20)
    driver.get(url)
    tr = driver.find_elements_by_xpath("/html/body/div/table/tbody/tr")

    result = []
    for row in tr:
        i = 0
        tds = row.find_elements_by_tag_name('td')
        for data in tds:
            if i == 0:
                result.append([data.text, data.find_element_by_tag_name('a').get_attribute("href")])
            i += 1
    driver.quit()
    return result
