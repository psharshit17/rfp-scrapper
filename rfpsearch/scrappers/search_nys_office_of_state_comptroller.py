from bs4 import BeautifulSoup
from beautifulscraper import urllib2


def scrap():
    results = []
    url = 'https://www.osc.state.ny.us/procurement/'
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')
    tr = tidy.find_all('tr')
    fin = dict()
    description, link = 0, 0
    for row in tr:
        td = row.find_all('td')
        i = 0
        for data in td:
            i += 1
            if i == 4:
                description = data.text
            elif i == 5:
                if data.text == 'Request Info':
                    link = data.find('a').get('href')
                else:
                    description = 0
        if description != 0:
            fin[description] = link
            results += [[description, link]]
    return results
