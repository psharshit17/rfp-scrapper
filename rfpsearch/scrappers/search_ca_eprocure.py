from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC


def scrap():
    url = 'https://caleprocure.ca.gov/pages/Events-BS3/event-search.aspx'
    # load new url using selenium
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    wait = WebDriverWait(driver, 20)
    driver.get(url)

    wait.until(EC.presence_of_element_located((By.XPATH, '//*[@id="datatable-ready"]/tbody')))
    results = []
    fin = dict()
    description, link = 0, 0
    startdate, enddate = 'NA', 'NA'
    status = 'NA'
    tr = driver.find_elements_by_xpath('//*[@id="datatable-ready"]/tbody/tr')
    for row in tr:
        td = row.find_elements_by_tag_name('td')
        i=0
        for data in td:
            i+=1
            if(i==3):
                description = data.text
            if(i==5):
                enddate = data.text
            if(i==6):
                status = data.text
            link=url

        if(status == 'Posted'):
            fin[description] = link
            results += [[description, link, startdate, enddate]]
            description, link = 0, 0
            startdate, enddate = 'NA', 'NA'
            status = 'NA'
    driver.quit()
    return results

