from bs4 import BeautifulSoup
from urllib.request import urlopen


def scrap():
    results = []
    url = "http://www.panynj.gov/business-opportunities/bid-proposal-advertisements.html"
    content = urlopen(url)
    soup = BeautifulSoup(content, "html.parser")
    table_div = soup.find("div", {"id": "simpleTabs-content-6"})
    tbody = table_div.find("tbody")
    trs = tbody.findAll("tr", recursive=False)
    for tr in trs:
        tds = tr.findAll("td", recursive=False)
        description = tds[2].find("p").getText()
        link = "http://www.panynj.gov/business-opportunities/" + tds[3].find("a").attrs['href']
        results += [[description, link]]
    return results
