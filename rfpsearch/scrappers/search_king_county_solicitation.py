# Created by lchhabriya at 10/18/2018

import xlrd
from selenium import webdriver  # To launch/initialise a browser
from selenium.webdriver.support.wait import WebDriverWait  # To wait for a page to load.

def scrap():
    workbook = xlrd.open_workbook("third_party_tools\\Login_Information.xlsx")
    sheet = workbook.sheet_by_name("Sheet1")  # Read data from Excel sheet named "Sheet1"
    rowcount = sheet.nrows  # Get number of rows with data in excel sheet
    colcount = sheet.ncols  # Get number of columns with data in each row. Returns highest number

    results = []
    fin = dict()
    description, link = 0, 0
    startdate, enddate = 'NA', 'NA'
    username = 'NA'
    paswwrod = 'NA'
    for curr_row in range(1, rowcount, 1):
        for curr_col in range(1, colcount, 1):
            data = sheet.cell_value(curr_row, curr_col)  # Read the data in the current cell
            if (data == 'King County Solicitation'):
                username1 = sheet.cell_value(curr_row, curr_col + 1)
                password1 = sheet.cell_value(curr_row, curr_col + 2)

    url = 'https://procurement.kingcounty.gov/procurement_ovr/default.aspx'
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    wait = WebDriverWait(driver, 20)
    driver.get(url)

    username = driver.find_element_by_xpath('//*[@id="kingcounty_gov_cphContent_txtUsername"]')
    password = driver.find_element_by_xpath('//*[@id="kingcounty_gov_cphContent_txtPassword"]')

    username.send_keys(username1)
    password.send_keys(password1)

    driver.find_element_by_css_selector("#kingcounty_gov_cphContent_btnLogin").click()
    driver.find_element_by_xpath('//*[@id="kingcounty_gov_cphContent_dlTypMenu_ctl02_lbMenuItem"]').click()
    # driver.find_element_by_xpath('//*[@id="kingcounty_gov_cphContent_dlTypMenu_ctl01_lbMenuItem"]').click()
    tbody = driver.find_element_by_xpath('//*[@id="kingcounty_gov_cphContent_GridView1"]/tbody')
    tr = tbody.find_elements_by_tag_name('tr')
    # for row in tr:
    td = tr[0]
    div = td.find_element_by_tag_name('div')
    divbiddetails = div.find_element_by_class_name('bid-listitem-detail')
    divhref = divbiddetails.find_element_by_tag_name('a')
    link = divhref.get_attribute('href')
    description = divhref.text
    divbidstatus = td.find_element_by_class_name('bid-listitem-status')
    table = divbidstatus.find_element_by_tag_name('table')
    tbody = table.find_element_by_tag_name('tbody')
    tr = tbody.find_element_by_tag_name('tr')
    td = tr.find_element_by_tag_name('td')
    enddate = td.text.strip()
    enddate = enddate[10:]


    if (description != 0):
        fin[description] = link
        results += [[description, link, startdate, enddate]]
        description, link = 0, 0
        startdate, enddate = 'NA', 'NA'
    driver.find_element_by_xpath('//*[@id="kingcounty_gov_cphContent_Welcome1_logout"]').click()
    #     # driver.find_element_by_xpath('/html/body/table/tbody/tr[2]/td[2]/p/span/a[4]').click()
    driver.quit()
    return results