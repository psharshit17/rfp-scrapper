# Created by lchhabriya at 10/10/2018

from selenium import webdriver  # To launch/initialise a browser
from selenium.webdriver.support.wait import WebDriverWait  # To wait for a page to load.
from bs4 import BeautifulSoup  # scrapping
from selenium.common.exceptions import NoSuchElementException  # Exception Handling

def scrap():
    results = []
    url = "https://www.nycedc.com/opportunities/real-estate-development-procurement?category=1815"
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    wait = WebDriverWait(driver, 20)
    driver.get(url)
    tidy = BeautifulSoup(driver.page_source, 'html.parser')
    div = tidy.findAll('div', {"class": "node-teaser rfp"})
    fin = dict()
    description, link = 0, 0
    startdate,submissiondeadline = 'NA','NA'
    for row in div:
        h4 = row.find('h4')
        link = "https://www.nycedc.com" + h4.find('a').get('href')
        description = h4.text
        h5 = row.find('h5')
        submissiondeadline = h5.text
        if description != 0 and link != 0:
            fin[description] = link
            results += [[description, link, startdate, submissiondeadline[21:]]]
            description, link = 0, 0
            startdate, submissiondeadline = 'NA', 'NA'
    driver.quit()
    return results


