from bs4 import BeautifulSoup
from urllib.request import urlopen

def scrap():
    result = []
    for i in range(1000):
        url = "https://esd.ny.gov/doing-business-ny/requests-proposals?page=" + str(i)
        content = urlopen(url)
        soup = BeautifulSoup(content, "html.parser")
        # totalitems = soup.find("div", {"class": "custom-media-header"}) this is to get the total number of articles
        table_div = soup.findAll("div", {"class": "result-card-headline"})
        if table_div:
            for tag in table_div:
                description = tag.find("a").getText()
                link = "https://esd.ny.gov/" + tag.find("a").attrs['href']
                result += [[description, link]]
        else:
             break

        #put some condition to break out of loop before it searches for 1000 pages
        # if totalitems == tag:
        #     break

    return result
