# Created by lchhabriya at 10/10/2018
from selenium import webdriver  # To launch/initialise a browser
from selenium.webdriver.support.wait import WebDriverWait  # To wait for a page to load.
from bs4 import BeautifulSoup  # scrapping


def scrap():
    results = []
    fin = dict()
    description, link = 0, 0
    bidstartdate, bidenddate = 'NA', 'NA'
    url = 'https://a841-dotwebpcard01.nyc.gov/RFP/'
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    wait = WebDriverWait(driver, 20)
    driver.get(url)
    # page = urllib2.urlopen(url)
    # tidy = BeautifulSoup(page, 'html.parser')
    tidy = BeautifulSoup(driver.page_source, 'html.parser')
    div = tidy.find('div',{"class":"sub1"})
    table = tidy.find('table',{"class":"sortable"})
    tbody = table.find('tbody')
    tr = tbody.findAll('tr')
    for row in tr:
        td = row.findAll('td')
        i=0
        for data in td:
            i+=1
            if (i == 2):
                description = data.text.strip()
                link = "https://a841-dotwebpcard01.nyc.gov/RFP/" + data.find('a').get('href')
            if (i == 4):
                bidstartdate = data.text.strip()
            if (i == 5):
                bidenddate = data.text.strip()
        if description != 0:
            fin[description] = link
            results += [[description, link, bidstartdate, bidenddate]]
            description, link = 0, 0
            bidstartdate, bidenddate = 'NA', 'NA'
    driver.quit()
    return results


