# Created by lchhabriya at 10/12/2018

from bs4 import BeautifulSoup
from beautifulscraper import urllib2


def scrap():
    results = []
    fin = dict()
    description, link = 0, 0
    bidstartdate, bidenddate = 'NA', 'NA'
    url = 'https://hampton.gov/Bids.aspx?CatID=showStatus&txtSort=Category&showAllBids=&Status=open'
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')
    table = tidy.find('table',{"summary":"Bid Items"})
    tr = table.findAll('tr')
    tr = tr[1:]
    description, link = 0, 0
    bidstartdate, bidenddate = 'NA', 'NA'
    status = 'NA'
    for row in tr:
        i = 0
        td = row.findAll('td')
        for data in td:
            i += 1
            if (i == 2):
                span = data.find('span')
                link = "https://hampton.gov/" + span.find('a').get('href')
                description = span.text.strip()
            if (i == 4):
                span = data.findAll('span')
                status = span[0].text
                bidenddate = span[1].text
                bidenddate = bidenddate[:10].strip()
        if status == 'Open':
            fin[description] = link
            results += [[description, link, bidstartdate, bidenddate]]
            description, link = 0, 0
            bidstartdate, bidenddate = 'NA', 'NA'
            status = 'NA'
    return results
