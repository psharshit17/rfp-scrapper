# Created by lchhabriya at 10/19/2018

import xlrd
from selenium import webdriver  # To launch/initialise a browser
from selenium.webdriver.support.wait import WebDriverWait  # To wait for a page to load.
from bs4 import BeautifulSoup  # scrapping

def scrap():
    results = []
    fin = dict()
    description, link = 0, 0
    startdate, enddate = 'NA', 'NA'
    username = 'NA'
    paswwrod = 'NA'
    category = 'NA'

    workbook = xlrd.open_workbook("third_party_tools\\Login_Information.xlsx")
    sheet = workbook.sheet_by_name("Sheet1")  # Read data from Excel sheet named "Sheet1"
    rowcount = sheet.nrows  # Get number of rows with data in excel sheet
    colcount = sheet.ncols  # Get number of columns with data in each row. Returns highest number
    for curr_row in range(1, rowcount, 1):
        for curr_col in range(1, colcount, 1):
            data = sheet.cell_value(curr_row, curr_col)  # Read the data in the current cell
            if (data == 'Los Angeles Unified School District'):
                username1 = sheet.cell_value(curr_row, curr_col + 1)
                password1 = sheet.cell_value(curr_row, curr_col + 2)

    url = 'http://psd.lausd.net/vendors'
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    wait = WebDriverWait(driver, 20)
    driver.get(url)

    username = driver.find_element_by_xpath('//*[@id="UserName"]')
    password = driver.find_element_by_xpath('//*[@id="Password"]')
    username.send_keys(username1)
    password.send_keys(password1)
    driver.find_element_by_xpath('//*[@id="LogInSubmit"]').click()
    url = 'https://psd.lausd.net/vendors/RFPList.aspx?RFPStatus=Current'
    driver.get(url)
    tidy = BeautifulSoup(driver.page_source, 'html.parser')

    table = tidy.find('table', {"id": "CurrentList"})
    tr = table.findAll('tr')
    tr = tr[1:]
    for row in tr:
        td = row.findAll('td')
        i = 0
        for data in td:
            i += 1
            if (i == 1):
                link = "https://psd.lausd.net/vendors/" + data.find('a').get('href')
            if (i == 2):
                description = data.text.strip()
            if (i == 3):
                startdate = data.text.strip()
            if (i == 4):
                enddate = data.text.strip()
        if (description !=0):
            fin[description] = link
            results += [[description, link, startdate, enddate]]
            description, link = 0, 0
            startdate, enddate = 'NA', 'NA'
    driver.quit()
    return results