# Created by lchhabriya at 10/26/2018

from bs4 import BeautifulSoup
from beautifulscraper import urllib2


def scrap():
    results = []
    url = 'http://www.wsdot.wa.gov/biz/contaa/Contracts/default.htm#current'
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')
    div = tidy.find('div',{"id":"main"})
    li = div.findAll('li')
    fin = dict()
    description, link = 0, 0
    bidstartdate, bidenddate = 'NA', 'NA'
    for row in li:
        link = row.find('a').get('href')
        if ('http://www.wsdot.wa.gov/biz/' not in link):
            link = 'http://www.wsdot.wa.gov/biz/contaa/Contracts/' + link
        description = row.text
        if(description !=0):
            fin[description] = link
            results += [[description, link, bidstartdate, bidenddate]]
            description, link = 0, 0
            bidstartdate, bidenddate = 'NA', 'NA'
    return results



