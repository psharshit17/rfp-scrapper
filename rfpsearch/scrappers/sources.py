import time
import traceback
import threading
import math
from nltk.stem import LancasterStemmer
from . import search_new_york_power_authority
from . import search_nys_office_of_general_services
from . import search_gosr
from . import search_ny_empire_state_development
from . import search_port_authority_of_new_york_and_new_jersey
from . import search_nys_office_of_state_comptroller
from . import search_cali_valley_transportation_authority
from . import search_san_francisco_water_power_sewer
from . import search_camment
from . import search_city_county_san_francisco
from . import search_golden_gate_bridge
from . import search_county_of_san_luis_obispo
#from . import search_san_francisco_department_of_public_health
from . import search_jacksonville_transportation_authority_procurment
from . import search_city_of_san_antonio
from . import search_north_texas_tollway_authority
from . import search_san_antonio_water_system
from . import search_ride_metro
from . import search_pierce_transit
from . import search_njstart
from . import search_nys_cio_oft
from . import search_nyc_economic_development_corporation
from . import search_cuny
from . import search_united_nations_development_programme
from . import search_nyc_dot
from . import search_city_of_philadelphia_procurement_department
from . import search_nj_turnpike
from . import search_procure_az_gov
from . import search_la_bavn
from . import search_smud
from . import search_district_of_columbia_esolicitation
from . import search_hampton_procurement_office
from . import search_wmata
# from . import search_niagara_frontier_transportation_authority
from . import search_dc_water
from . import search_king_county_solicitation
from . import search_metropolitan_atlanta_rapid_transit_authority
from . import search_los_angeles_unified_school_district
from . import search_samtrans
from . import search_fulton_county_school
from . import search_city_of_atlanta_online_fulton_county
from . import search_commbuys
from . import search_ca_eprocure
from . import search_los_anglas_county
from . import search_econtract_philly
from . import search_the_department_of_treasure_of_nj
from . import search_wa_state_department_of_transportation
from . import search_dallas_fort_worth_international_airport
from . import search_via_metro_transit
from . import search_tx_dart
from . import search_mwaa
from . import search_city_record_of_nyc
from . import search_sfdph
from . import search_port_of_seattle
from . import search_sound_transit
from . import search_city_of_seattle_procurement
from . import search_regional_transportation_commission
from . import search_lane_trasit_district_procurement
from . import search_penn_bid
from . import search_wssc_one_Source
#from . import search_rockland_county
from . import search_massport


def get_sources():
    category = True
    sources = {
        'c1': ["New York (NY)", None, category],
        0: ["Port Authority of New York and New Jersey", search_port_authority_of_new_york_and_new_jersey.scrap,
            not category],
        1: ["New York Power Authority", search_new_york_power_authority.scrap, not category],
        2: ["NYS Office of General Services", search_nys_office_of_general_services.scrap, not category],
        3: ["NYS Office of State Comptroller", search_nys_office_of_state_comptroller.scrap, not category],
        4: ["GOSR", search_gosr.scrap, not category],
        5: ["NY Empire State Development", search_ny_empire_state_development.scrap, not category],
        6: ["NYS CIO OFT", search_nys_cio_oft.scrap, not category],
        7: ["NYC Economic Development Corporation", search_nyc_economic_development_corporation.scrap, not category],
        8: ["CUNY", search_cuny.scrap, not category],
        9: ["United Nations Development Programme", search_united_nations_development_programme.scrap, not category],
        10: ["NYC DOT", search_nyc_dot.scrap, not category],
        11: ["City Record of NYC", search_city_record_of_nyc.scrap, not category],
        #12: ["Rockland County", search_rockland_county.scrap, not category],
        # 11: ["Niagara Frontier Transportation Authority", search_niagara_frontier_transportation_authority.scrap, not category],

        'c2': ["New Jersey (NJ)", None, category],
        101: ["NJ - NJSTART", search_njstart.scrap, not category],
        102: ["NJ - NJ Turnpike", search_nj_turnpike.scrap, not category],
        103: ["The Department of Treasure of NJ", search_the_department_of_treasure_of_nj.scrap, not category],

        'c3': ["Arizona (AZ)", None, category],
        301: ["AZ - Procure AZ GOV", search_procure_az_gov.scrap, not category],

        'c4': ["California (CA)", None, category],
        401: ["Santa Clara Valley Transportation Authority", search_cali_valley_transportation_authority.scrap,
              not category],
        402: ["SFWATER", search_san_francisco_water_power_sewer.scrap, not category],
        403: ["Orange County Transportation Authority", search_camment.scrap, not category],
        404: ["City and County of San Francisco", search_city_county_san_francisco.scrap, not category],
        405: ["Goldgate", search_golden_gate_bridge.scrap, not category],
        406: ["County of San Luis Obispo", search_county_of_san_luis_obispo.scrap, not category],
        407: ["LA BAVN/LAWA", search_la_bavn.scrap, not category],
        408: ["SMUD", search_smud.scrap, not category],
        409: ["SFDPH", search_sfdph.scrap, not category],
        4010: ["Los Angeles Unified School District", search_los_angeles_unified_school_district.scrap, not category],
        4011: ["SAMTRANS", search_samtrans.scrap, not category],
        4012: ["CA eProcure", search_ca_eprocure.scrap, not category],
        4013: ["Los Anglas County", search_los_anglas_county.scrap, not category],

        # 'c5': ["Colorado (CO)", None, category],
        # 'c6': ["Connecticut (CT)", None, category],
        'c7': ["District of Columbia (DC)", None, category],
        701: ["WMATA", search_wmata.scrap, not category],
        702: ["District of Columbia e-Solicitation", search_district_of_columbia_esolicitation.scrap, not category],
        703: ["DC Water", search_dc_water.scrap, not category],
        704: ["MWAA", search_mwaa.scrap, not category],
        # 'c8': ["Delaware (DE)", None, category],

        'c9': ["Florida (FL)", None, category],
        901: ["JTA Procurement", search_jacksonville_transportation_authority_procurment.scrap, not category],

        'c10': ["Georgia (GA)", None, category],
        1001: ["Metropolitan Atlanta Rapid Transit Authority",
               search_metropolitan_atlanta_rapid_transit_authority.scrap, not category],
        1002: ["Fulton County school", search_fulton_county_school.scrap, not category],
        1003: ["City of Atlanta online-Fulton County", search_city_of_atlanta_online_fulton_county.scrap, not category],
        # 'c11': ["Hawaii", None, category],
        # 'c12': ["Iowa (IA)", None, category],
        # 'c13': ["Idaho (ID)", None, category],
        # 'c14': ["Illinois (IL)", None, category],
        # 'c15': ["Kentucky (KY)", None, category],
        'c16': ["Massachusetts (MA)", None, category],
        1601: ["COMMBUYS", search_commbuys.scrap, not category],
        1602: ["Massport", search_massport.scrap, not category],
        'c17': ["Maryland (MD)", None, category],
        1701: ["WSSC One-Source", search_wssc_one_Source.scrap, not category],
        # 'c18': ["Minnesota (MN)", None, category],
        # 'c19': ["Missouri (MO)", None, category],
        # 'c20': ["North Carolina (NC)", None, category],
        # 'c21': ["North Dakota (ND)", None, category],
        # 'c22': ["New Hamshire (NH)", None, category],
        'c23': ["Nevada (NV)", None, category],
        2301: ["Regional Transportation Commission", search_regional_transportation_commission.scrap, not category],
        # 'c24': ["Ohio (OH)", None, category],
        # 'c25': ["Oklahoma (OK)", None, category],
        'c26': ["Oregon (OR)", None, category],
        2601: ["Lane Trasit District Procurement Portal", search_lane_trasit_district_procurement.scrap, not category],
        'c27': ["Pennsylvenia (PA)", None, category],
        2701: ["City of Philadelphia Procurement Department",
               search_city_of_philadelphia_procurement_department.scrap, not category],
        2702: ["eContract Philly", search_econtract_philly.scrap, not category],
        2703: ["Penn Bid", search_penn_bid.scrap, not category],
        # 'c28': ["Rhode Island (RI)", None, category],
        # 'c29': ["South Carolina (SC)", None, category],
        # 'c30': ["South Dakota (SD)", None, category],
        # 'c31': ["Tennessee (TN) ", None, category],

        'c32': ["Texas (TX)", None, category],
        3201: ["City of San Antonio", search_city_of_san_antonio.scrap, not category],
        3202: ["North Texas Tollway Authority", search_north_texas_tollway_authority.scrap, not category],
        3203: ["San Antonio Water System", search_san_antonio_water_system.scrap, not category],
        3204: ["Ride Metro", search_ride_metro.scrap, not category],
        3205: ["Dallas/Fort Worth International Airport", search_dallas_fort_worth_international_airport.scrap,
               not category],
        3206: ["Via Metro Transit", search_via_metro_transit.scrap, not category],
        3207: ["TX Dart", search_tx_dart.scrap, not category],

        # 'c33': ["Utah (UT)", None, category],
        'c34': ["Virgina (VA)", None, category],
        3401: ["Hampton Procurement Office", search_hampton_procurement_office.scrap, not category],

        'c35': ["Washington (WA)", None, category],
        3501: ["Pierce Transit", search_pierce_transit.scrap, not category],
        3502: ["King County Solicitation", search_king_county_solicitation.scrap, not category],
        3503: ["WA State Department of Transportation", search_wa_state_department_of_transportation.scrap,
               not category],
        3504: ["Port of Seattle", search_port_of_seattle.scrap, not category],
        3506: ["Sound Transit", search_sound_transit.scrap, not category],
        3507: ["City of Seattle Procurement", search_city_of_seattle_procurement.scrap, not category],

        # 'c36': ["Wisconsin (WI)", None, category],
    }
    return sources


def search(requested_sources):
    start_time = time.time()
    results = {}
    keywords = requested_sources['query'].lower().strip().split()
    del requested_sources['query']
    for index, is_included in requested_sources.items():
        if is_included:
            source_name = get_sources()[int(index)][0]
            scrap_function = get_sources()[int(index)][1]
            results[source_name] = filter_results(scrap_function(), keywords)
    print("--- %s seconds ---" % (time.time() - start_time))
    return results


def threaded_search(requested_sources):
    start_time = time.time()
    results = {}
    keywords = stem_words(requested_sources['query'].lower().strip().split())
    del requested_sources['query']
    source_indices = []
    for index, is_included in requested_sources.items():
        if is_included:
            source_indices += [index]
            results[get_sources()[int(index)][0]] = []
    sources_per_thread = 5
    total_threads = math.ceil(len(source_indices) * 1.0 / sources_per_thread)
    #sources_per_thread = math.ceil(len(source_indices) * 1.0 / total_threads)
    start_index = 0
    end_index = min(sources_per_thread, len(source_indices))
    threads = []
    for thread in range(total_threads):
        threads += [threading.Thread(target=search_thread, args=(start_index, end_index, source_indices, keywords, results))]
        start_index = end_index
        end_index = min(end_index + sources_per_thread, len(source_indices))
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()
    print("--- %s seconds ---" % (time.time() - start_time))
    return results


def search_thread(start_index, end_index, source_indices, keywords, results):
    index = start_index
    while index < end_index:
        source_index = source_indices[index]
        source_name = get_sources()[int(source_index)][0]
        scrap_function = get_sources()[int(source_index)][1]
        try:
            results[source_name] = filter_results(scrap_function(), keywords)
        except Exception as e:
            results[source_name] = [["Issue Accessing the Website. Please contact administrator.", "#"]]
            traceback.print_exc()
            print(e)
        index += 1


def filter_results(scrapped_results, keywords):
    if len(keywords) is 0:
        return scrapped_results
    results = []
    for result in scrapped_results:
        description = result[0].lower()
        for keyword in keywords:
            if keyword in description:
                results += [result]
                break
    return results


def stem_words(words):
    """Stem words in list of tokenized words"""
    stemmer = LancasterStemmer()
    stems = []
    for word in words:
        stem = stemmer.stem(word)
        stems.append(stem)
    return stems
