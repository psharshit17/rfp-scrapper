# Created by lchhabriya at 10/15/2018

from bs4 import BeautifulSoup
from beautifulscraper import urllib2

def scrap():
    results = []
    fin = dict()
    description, link = 0, 0
    bidstartdate, bidenddate = 'NA', 'NA'
    url = 'https://www.wmata.com/Business/procurement/solicitations/active-procurement-opportunities.cfm'
    page = urllib2.urlopen(url)
    tidy = BeautifulSoup(page, 'html.parser')
    table = tidy.find('table',{"class":"table-responsive-stacked"})
    tbody = table.find('tbody')
    tr = tbody.findAll('tr')
    for row in tr:
        i = 0
        td = row.findAll('td')
        for data in td:
            i += 1
            if(i==1):
                link = "https://www.wmata.com/Business/procurement/solicitations/" + data.find('a').get('href')
            if(i==2):
                description = data.text.strip()
            if(i==3):
                bidstartdate = data.text
            if(i==5):
                bidenddate = data.text
        if description != 0:
                fin[description] = link
                results += [[description, link, bidstartdate, bidenddate]]
                description, link = 0, 0
                bidstartdate, bidenddate = 'NA', 'NA'
    return results
