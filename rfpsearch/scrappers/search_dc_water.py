# Created by lchhabriya at 10/17/2018

import xlrd
from selenium import webdriver  # To launch/initialise a browser
from selenium.webdriver.support.wait import WebDriverWait  # To wait for a page to load.
from bs4 import BeautifulSoup  # scrapping


def scrap():
    results = []
    fin = dict()
    description, link = 0, 0
    startdate,enddate = 'NA','NA'
    username = 'NA'
    paswwrod = 'NA'

    workbook = xlrd.open_workbook("third_party_tools\\Login_Information.xlsx")
    sheet = workbook.sheet_by_name("Sheet1") #Read data from Excel sheet named "Sheet1"
    rowcount = sheet.nrows #Get number of rows with data in excel sheet
    colcount = sheet.ncols #Get number of columns with data in each row. Returns highest number

    for curr_row in range(1, rowcount, 1):
        for curr_col in range(1, colcount, 1):
            data = sheet.cell_value(curr_row, curr_col) # Read the data in the current cell
            if(data=='DC Water'):
                username1 =  sheet.cell_value(curr_row, curr_col+1)
                password1 = sheet.cell_value(curr_row, curr_col+2)

    url = 'https://vendor.dcwater.com/'
    driver = webdriver.Chrome(executable_path="third_party_tools\\chromedriver_win32\\chromedriver")
    wait = WebDriverWait(driver, 20)
    driver.get(url)

    username = driver.find_element_by_xpath('//*[@id="username"]')
    password = driver.find_element_by_xpath('//*[@id="password"]')

    username.send_keys(username1)
    password.send_keys(password1)

    driver.find_element_by_css_selector("#b2").click()
    driver.find_element_by_xpath('/html/body/table/tbody/tr[2]/td[2]/a').click()
    driver.find_element_by_xpath('//*[@id="globalnav"]/li[1]/a/span').click()
    driver.find_element_by_xpath('//*[@id="globalnav"]/ul/li[1]/a/span').click()

    tidy = BeautifulSoup(driver.page_source, 'html.parser')
    table = tidy.find('table', {"class": "general"})
    tr = table.findAll('tr')
    tr = tr[1:]
    for row in tr:
        td = row.findAll('td')
        i=0
        for data in td:
            i += 1
            if(i == 2):
                description = data.text.strip()
            if(i==4):
                link = data.find('a').get('href')
                link = "https://vendor.dcwater.com" + link[1:]
        if(description != 0):
            fin[description] = link
            results += [[description, link, startdate, enddate]]
            description, link = 0, 0
            startdate, enddate = 'NA', 'NA'
    driver.find_element_by_xpath('/html/body/table/tbody/tr[2]/td/strong/a').click()
    driver.find_element_by_xpath('/html/body/table/tbody/tr[2]/td[2]/p/span/a[4]').click()
    driver.quit()
    return results