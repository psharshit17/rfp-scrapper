from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from .scrappers import sources
import json

# Create your views here.


@csrf_exempt
def index(request):
    if request.method == 'GET':
        context = {"sources": sources.get_sources()}
        return render(request, 'rfpsearch/index.html', context)
    elif request.method == 'POST':
        results = sources.threaded_search(json.loads(request.body))
        return JsonResponse(results)
