app = angular.module("rfpsearch", []);

app.controller("searchCtrl", function($scope, $http) {
    $scope.sources = {}; $scope.sources.query = "";
    $scope.categories = {};
    $scope.sourcesStructure = {}; $scope.currentCategory = "Non";
    $scope.results = {};
    $scope.showSpinner = false;

    getCookie = function(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = cookies[i].trim();
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    };

    $scope.submitQuery = function() {
        $scope.results = {};
        $scope.showSpinner = true;
        config = {headers: {'Content-Type': 'application/json', 'X-CSRFToken': getCookie('csrftoken')}};
        $http.post("/rfpsearch/", $scope.sources, config)
        .then(function(response){
            $scope.showSpinner = false;
            $scope.results = response["data"]
        },
        function(response){
            $scope.showSpinner = false;
            alert("Error fetching results. Please contact administrator.");
        });
    };

    $scope.checkAllAgencies = function() {
        angular.forEach($scope.sources, function(value, key) {
            if(key != "query")
                $scope.sources[key] = true;
        });
    };

    $scope.uncheckAllAgencies = function() {
        angular.forEach($scope.sources, function(value, key) {
            if(key != "query")
                $scope.sources[key] = false;
        });
    };

    $scope.checkAllInCategory = function(category) {
        angular.forEach($scope.sourcesStructure[category], function(value) {
            $scope.sources[value] = ($scope.categories[category] ? true : false);
        });
    };
});